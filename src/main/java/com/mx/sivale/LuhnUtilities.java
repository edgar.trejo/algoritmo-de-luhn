package com.mx.sivale;

import java.util.logging.Logger;

public class LuhnUtilities {

    /*IMPORTANTE el numero de tarjeta debe incluir el codigo verificador*/
    public static boolean validCreditCard(String card) {
        if (card == null)
            return false;
        char checkDigit = card.charAt(card.length() - 1);
        String digit = calculateCheckDigit(card.substring(0, card.length() - 1));
        return checkDigit == digit.charAt(0);
    }

    public static String calculateCheckDigit(String cardNumber) {
        if (cardNumber == null)
            return null;
        String digit;
        /* convertimos los digitos de la tarjeta en un arreglo de enteros  */
        int[] digits = new int[cardNumber.length()];
        for (int i = 0; i < cardNumber.length(); i++) {
            digits[i] = Character.getNumericValue(cardNumber.charAt(i));
        }

        /* Recorremos cada segundo digito de la tarejta */
        for (int i = digits.length - 1; i >= 0; i -= 2)	{

            /*Duplicamos cada segundo digito*/
            digits[i] *= 2;

            /* Si el digito duplicado es mayor a 10 - si es el caso restamos 9 */
            if (digits[i] >= 10) {
                digits[i] = digits[i] - 9;
            }
        }
        int sum = 0;
        for (int i = 0; i < digits.length; i++) {
            sum += digits[i]; /*Sumamos todos los digitos*/
        }
        /* multiplicamos por 9*/
        sum = sum * 9;

        /* convertimosla sumatoria a String */
        digit = sum + "";
        return digit.substring(digit.length() - 1);// extraemos el ultimo caracter de la sumatoria
    }

    public static void main(String[] args)	{
        Logger logger = Logger.getLogger(LuhnUtilities.class.getName());
        String pan1 = "37828224631000";
        logger.info("utilidades de Luhn");

        logger.info("Calculado digito verificador para: " + pan1);
        String digit = calculateCheckDigit(pan1);
        logger.info("Check digit: " + digit);

        logger.info("Validando numero de tarjeta " + pan1 + " SIN codigo verificador :'" + validCreditCard(pan1));
        pan1 = pan1 + digit;
        logger.info("Validando numero de tarjeta " + pan1 + " CON codigo verificador :'" + validCreditCard(pan1));
    }

}
